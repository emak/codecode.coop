.PHONY: install build test run
.DEFAULT_GOAL := run

install:
	@command -v bundle >/dev/null 2>&1 || gem install bundler
	bundle check || bundle install

build: install
	bundle exec jekyll build -d public

test: install
	bundle exec jekyll build -d test

run: install
	bundle exec jekyll serve --livereload
